/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package grama.classes;

import grama.classes.models.Graphe;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author nabil
 */
public class GrapheTest {
    /**
     * Test of plusOuverte method, of class Graphe.
     */
    @Test
    public void testPlusOuverte() {
        System.out.println("plusOuverte");
        Graphe instance = new Graphe("SAE GRAMA.csv");
        String expResult = "";
        String result = instance.plusOuverte("mahajanga", "DS Mandoky");
        assertEquals("mahajanga", result);
    }

    /**
     * Test of plusGastronomique method, of class Graphe.
     */
    @Test
    public void testPlusGastronomique() {
        System.out.println("plusGastronomique");
        Graphe instance = new Graphe("sae grama.csv");
        String result = instance.plusGastronomique("restoria", "swimming pool");
        assertEquals("Egaux", result);
    }

    /**
     * Test of plusCulturelle method, of class Graphe.
     */
    @Test
    public void testPlusCulturelle() {
        System.out.println("plusCulturelle");
        Graphe instance = new Graphe("sae grama.csv");
        String result = instance.plusCulturelle("dakar", "matam");
        assertEquals("dakar", result);
    }

    /**
     * Test of getIndiceNoeud method, of class Graphe.
     */
    @Test
    public void testGetIndiceNoeud() {
        System.out.println("getIndiceNoeud");
        Graphe instance = new Graphe("sae grama.csv");
        int expResult = 0;
        int result = instance.getIndiceNoeud("dakar");
        assertEquals(expResult, result);
    }

    /**
     * Test of counterNoeudByType method, of class Graphe.
     */
    @Test
    public void testCounterNoeudByType() {
        System.out.println("counterNoeudByType");
        Graphe instance = new Graphe("sae grama.csv");
        assertEquals(18, instance.counterNoeudByType(instance.getListeDeNoeuds(), "v"));
        assertEquals(6, instance.counterNoeudByType(instance.getListeDeNoeuds(), "r"));
        assertEquals(6, instance.counterNoeudByType(instance.getListeDeNoeuds(), "l"));
    }

    /**
     * Test of counterLinkByType method, of class Graphe.
     */
    @Test
    public void testCounterLinkByType() {
        System.out.println("counterLinkByType");
        Graphe instance = new Graphe("sae grama.csv");
        assertEquals(9, instance.counterLinkByType(instance.getListeDeNoeuds(), "a"));
        assertEquals(10, instance.counterLinkByType(instance.getListeDeNoeuds(), "n"));
        assertEquals(14, instance.counterLinkByType(instance.getListeDeNoeuds(), "d"));
    }

    /**
     * Test of connaitre2Distance method, of class Graphe.
     */
    @Test
    public void testConnaitre2Distance() {
        System.out.println("connaitre2Distance");
        Graphe instance = new Graphe("sae grama.csv");
        boolean result1 = instance.connaitre2Distance(instance.getListeDeNoeuds().get(0), instance.getListeDeNoeuds().get(3));
        assertEquals(true, result1);
        boolean result2 = instance.connaitre2Distance(instance.getListeDeNoeuds().get(0), instance.getListeDeNoeuds().get(2));
        assertEquals(false, result2);
        
    }
    
}
